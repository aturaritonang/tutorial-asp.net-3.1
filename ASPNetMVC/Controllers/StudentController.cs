﻿using ASPNetMVC.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNetMVC.Controllers
{
    public class StudentController : Controller
    {
        private static List<StudentViewModel> listStudent = new List<StudentViewModel>()
        {
            new StudentViewModel(){
                Id = 1,
                FirstName = "Atur",
                LastName = "Aritonang",
                Gender = "Male",
                BirthDate = new DateTime(1999, 10, 12)
            },
            new StudentViewModel(){
                Id = 2,
                FirstName = "Wati",
                LastName = "Maria",
                Gender = "Female",
                BirthDate = new DateTime(2002, 1, 2)
            },
            new StudentViewModel(){
                Id = 3,
                FirstName = "Iwan",
                LastName = "Saroso",
                Gender = "Male",
                BirthDate = new DateTime(1997, 9, 11)
            },
        };
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Display()
        {
            StudentViewModel studentViewModel = new StudentViewModel()
            {
                Id = 1,
                FirstName = "Atur",
                LastName = "Aritonang",
                Gender = "Male",
                BirthDate = new DateTime(1999, 10, 12)
            };

            return View(studentViewModel);
        }

        public IActionResult ById(int id)
        {
            return View("_display", listStudent.Find(o => o.Id == id));
        }

        public IActionResult List()
        {
            return View(listStudent);
        }

    }
}
